//
//  ViewController.swift
//  MMAds-Example
//
//  Created by Guillem Budia Tirado on 07/06/2019.
//  Copyright © 2019 MonkingMe. All rights reserved.
//

import UIKit
import MMAds

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func showAdQuizButtonAction(_ sender: Any) {
        
        MonkingMeAds.showAdQuiz(in: self)
        
    }
    
}

